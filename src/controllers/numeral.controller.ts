import { Controller, Get, Param } from 'routing-controllers';
import NumeralService from '@services/numeral.service';
import { logger } from '@utils/logger';

@Controller()
export class NumeralController {
  numeralService = new NumeralService();

  @Get('/numeral/:numeral')
  async getRomanFromNumeral(@Param('numeral') numeral: number) {
    const roman: string = this.numeralService.getRomanFromNumeral(Number(numeral));
    return roman;
  }
}

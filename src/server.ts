import App from '@/app';
import { IndexController } from '@controllers/index.controller';
import { NumeralController } from '@controllers/numeral.controller';
import validateEnv from '@utils/validateEnv';

validateEnv();

const app = new App([IndexController, NumeralController]);
app.listen();

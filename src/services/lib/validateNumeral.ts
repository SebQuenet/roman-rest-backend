import { NumeralNotANumberError, NumeralTooLowError, NumeralTooHighError, NumeralNotAnIntegerError } from './errors';
import { MINIMUM_ALLOWED_NUMERAL, MAXIMUM_ALLOWED_NUMERAL } from './constants';

const validateNumeral = (numeralAsNumber: number): void => {
  if (isNaN(numeralAsNumber)) {
    throw NumeralNotANumberError;
  }
  if (!Number.isInteger(numeralAsNumber)) {
    throw NumeralNotAnIntegerError;
  }
  if (numeralAsNumber < MINIMUM_ALLOWED_NUMERAL) {
    throw NumeralTooLowError;
  }
  if (numeralAsNumber > MAXIMUM_ALLOWED_NUMERAL) {
    throw NumeralTooHighError;
  }
};

export default validateNumeral;

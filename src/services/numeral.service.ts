import { logger } from '@utils/logger';
import validateNumeral from './lib/validateNumeral';

const oneToNine = ['', 'I', 'II', 'III', 'IV', 'V', 'VI', 'VII', 'VIII', 'IX'];
const tenToNinety = ['', 'X', 'XX', 'XXX', 'XL', 'L', 'LX', 'LXX', 'LXXX', 'XC'];
const oneHundredToNineHundred = ['', 'C', 'CC', 'CCC', 'CD', 'D', 'DC', 'DCC', 'DCCC', 'CM'];
const oneToThreeThousands = ['', 'M', 'MM', 'MMM', '', '', '', '', '', ''];

class NumeralService {
  public getRomanFromNumeral(numeral: number): string {
    logger.debug('Entering getRomanFromNumeral');

    logger.debug('Validating numeral');
    validateNumeral(numeral);

    try {
      logger.debug('Isolate thousands');
      const thousands = Math.floor(numeral / 1000);
      const numeralHundreds = numeral - thousands * 1000;

      logger.debug('Isolate hundreds');
      const hundreds = Math.floor(numeralHundreds / 100);
      const numeralTens = numeralHundreds - hundreds * 100;

      logger.debug('Isolate tens');
      const tens = Math.floor(numeralTens / 10);

      logger.debug('Isolate units');
      const units = numeral % 10;

      logger.debug('Concatenate all parts of the roman number');
      const romanTranscription = oneToThreeThousands[thousands] + oneHundredToNineHundred[hundreds] + tenToNinety[tens] + oneToNine[units];

      return romanTranscription;
    } catch (error) {
      logger.error(error);
      throw error;
    }
  }
}
export default NumeralService;

import NumeralService from '@services/numeral.service';

describe('Testing numeral service', () => {
  let numeralService: NumeralService;

  beforeEach(() => {
    numeralService = new NumeralService();
  });

  const numeralToRomanSymbolsMapping = { 1: 'I', 5: 'V', 10: 'X', 50: 'L', 100: 'C', 500: 'D', 1000: 'M' };
  test.each(Object.entries(numeralToRomanSymbolsMapping))('should work for %s', (numeral, roman) => {
    expect(numeralService.getRomanFromNumeral(Number(numeral))).toEqual(roman);
  });

  it('should work for numbers beetween 1 and 9', () => {
    expect(numeralService.getRomanFromNumeral(1)).toEqual('I');
    expect(numeralService.getRomanFromNumeral(4)).toEqual('IV');
    expect(numeralService.getRomanFromNumeral(5)).toEqual('V');
    expect(numeralService.getRomanFromNumeral(9)).toEqual('IX');
  });

  it('should work for numbers beetween 10 and 99', () => {
    expect(numeralService.getRomanFromNumeral(10)).toEqual('X');
    expect(numeralService.getRomanFromNumeral(49)).toEqual('XLIX');
    expect(numeralService.getRomanFromNumeral(61)).toEqual('LXI');
    expect(numeralService.getRomanFromNumeral(99)).toEqual('XCIX');
  });

  it('should work for numbers beetween 100 and 999', () => {
    expect(numeralService.getRomanFromNumeral(100)).toEqual('C');
    expect(numeralService.getRomanFromNumeral(499)).toEqual('CDXCIX');
    expect(numeralService.getRomanFromNumeral(501)).toEqual('DI');
    expect(numeralService.getRomanFromNumeral(999)).toEqual('CMXCIX');
  });

  it('should pass when numeral is a valid number and throw an error whether numeral is not a valid number', () => {
    expect(() => {
      numeralService.getRomanFromNumeral(1);
    }).not.toThrow();
    expect(() => {
      numeralService.getRomanFromNumeral(1.5);
    }).toThrow();
    expect(() => {
      numeralService.getRomanFromNumeral(0);
    }).toThrow();
    expect(() => {
      numeralService.getRomanFromNumeral(5000);
    }).toThrow();
  });
});
